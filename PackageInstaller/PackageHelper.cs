﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PackageInstaller
{
    public class PackageHelper
    {
        private int inputCount = 0;
        private int packageNamesCount = 0;
        private string[] packageNames;
        private int loopCount = 0;
        private int maxLoopCount = 0;
        public string PackageNamesInOrder(string[] input)
        {
            string packageNamesInOrder = string.Empty;

            inputCount = input.Count(s => s != null);
            maxLoopCount = inputCount * (inputCount + 1);

            packageNames = new string[inputCount * 2 + 2];

            for (int i = 0; i < inputCount; ++i)
            {
                RecursionProcessInput(i, input, null);
                if (loopCount > maxLoopCount)
                {
                    break;
                }
            }

            if (loopCount < maxLoopCount)
            {
                foreach (string item in packageNames)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        if (packageNamesInOrder.Length > 0)
                            packageNamesInOrder += ", ";

                        packageNamesInOrder += item;
                    }
                }
            }

            return packageNamesInOrder;
        }

        private void RecursionProcessInput(int index, string[] input, string dependencyName)
        {
            loopCount++;

            if (loopCount > maxLoopCount)
            {
                return;
            }

            string[] packageAndDependency;

            if (index < inputCount)
            {
                packageAndDependency = input[index].Split(':');

                if (packageAndDependency.Length == 2)
                {
                    if (string.IsNullOrEmpty(dependencyName))
                    {
                        //if (packageAndDependency[1].Trim().Length > 0)
                        //{
                        //    RecursionProcessInput(0, input, packageAndDependency[1].Trim());
                        //    AddPackageName(packageAndDependency[0].Trim());
                        //}
                        //else
                        //{
                        //    AddPackageName(packageAndDependency[0].Trim());
                        //}

                        RecursionProcessHelper(input, packageAndDependency[0].Trim(), packageAndDependency[1].Trim());
                    }
                    else
                    {
                        if (packageAndDependency[0].Trim().Equals(dependencyName))
                        {
                            //if (packageAndDependency[1].Trim().Length > 0)
                            //{
                            //    RecursionProcessInput(0, input, packageAndDependency[1].Trim());
                            //    AddPackageName(packageAndDependency[0].Trim());
                            //}
                            //else
                            //{
                            //    AddPackageName(packageAndDependency[0].Trim());
                            //}
                            RecursionProcessHelper(input, packageAndDependency[0].Trim(), packageAndDependency[1].Trim());
                        }
                        else
                        {
                            RecursionProcessInput(index + 1, input, dependencyName);
                            AddPackageName(dependencyName);
                        }
                    }
                }
            }
        }

        private void RecursionProcessHelper(string[] input, string packageName, string dependencyName)
        {
            if (dependencyName.Length > 0)
            {
                RecursionProcessInput(0, input, dependencyName);
            }

            AddPackageName(packageName);
        }

        private void AddPackageName(string packageName)
        {
            bool packageNotAdded = true;

            for (int i = 0; i < packageNamesCount; ++i)
            {
                if (packageName.Equals(packageNames[i]))
                {
                    packageNotAdded = false;
                    break;
                }
            }

            if (packageNotAdded)
                packageNames[packageNamesCount++] = packageName;
        }
    }
}