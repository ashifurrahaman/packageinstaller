﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PackageInstaller.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestValidInput1()
        {
            string[] input = new string[10];

            input[0] = "KittenService: CamelCaser";
            input[1] = "CamelCaser:";

            PackageHelper objPackage = new PackageHelper();
            string packageNamesInOrder = objPackage.PackageNamesInOrder(input);
            Assert.AreEqual(packageNamesInOrder, "CamelCaser, KittenService");
        }

        [TestMethod]
        public void TestValidInput2()
        {
            string[] input = new string[10];

            input[0] = "KittenService:";
            input[1] = "Leetmeme: Cyberportal";
            input[2] = "Cyberportal: Ice";
            input[3] = "CamelCaser: KittenService";
            input[4] = "Fraudstream: Leetmeme";
            input[5] = "Ice:";

            PackageHelper objPackage = new PackageHelper();
            string packageNamesInOrder = objPackage.PackageNamesInOrder(input);
            Assert.AreEqual(packageNamesInOrder, "KittenService, Ice, Cyberportal, Leetmeme, CamelCaser, Fraudstream");
        }

        [TestMethod]
        public void TestInvalidInput()
        {
            string[] input = new string[10];

            input[0] = "KittenService:";
            input[1] = "Leetmeme: Cyberportal";
            input[2] = "Cyberportal: Ice";
            input[3] = "CamelCaser: KittenService";
            input[4] = "Fraudstream:";
            input[5] = "Ice: Leetmeme";

            PackageHelper objPackage = new PackageHelper();
            string packageNamesInOrder = objPackage.PackageNamesInOrder(input);
            Assert.AreEqual(packageNamesInOrder, "");
        }
    }
}
